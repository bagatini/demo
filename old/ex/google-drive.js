/*****

Bytutorial.com - online community to share articles for web and mobile programming and designers.
Author: Andy Suwandy

NOTE: Please change the CLIENT ID by creating your own app in google.
In order to work in your local computer, please change the client ID in the code and set the url of where the google drive app will be loaded.
otherwise you should get an error message saying the url you try to load does not match.

****/



/******************** GLOBAL VARIABLES ********************/



   var SCOPES            = [
                             'https://www.googleapis.com/auth/drive.file',
                           ];
   var CLIENT_ID         = '512065839586-cjv44bge384jthner9a3en6r5tvapu94.apps.googleusercontent.com';
   var API_KEY           = 'AIzaSyCWzj7-J_9fb9AASF3oqGLDJ-nG0Wws4cE';
   var FOLDER_NAME       = '';
   var FOLDER_ID         = 'root';
   var FOLDER_PERMISSION = true;
   var FOLDER_LEVEL      = 0;
   var NO_OF_FILES       = 1000;
   var DRIVE_FILES       = [];
   var FILE_COUNTER      = 0;
   var FOLDER_ARRAY      = [];



/******************** AUTHENTICATION ********************/



   function handleClientLoad()
   {
     // Load the API client and auth2 library
     gapi.load('client:auth2', initClient);
   }



   // authorize apps
   function initClient()
   {
     gapi.client.init
     (
       {
         // apiKey: API_KEY, //THIS IS OPTIONAL AND WE DONT ACTUALLY NEED THIS, BUT I INCLUDE THIS AS EXAMPLE
         clientId: CLIENT_ID,
         scope: SCOPES.join(' ')
       }
     ).then(
             function()
             {
               // Listen for sign-in state changes.
               gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
               // Handle the initial sign-in state.
               updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
             }
           );
   }



   // check the return authentication of the login is successful, we display the drive box and hide the login box.
   function updateSigninStatus(isSignedIn)
   {
     if (isSignedIn)
     {
       getDriveFiles();
     }
     else
     {
       $('#login-box').show();
     }
   }



   function handleAuthClick(event)
   {
     gapi.auth2.getAuthInstance().signIn();
   }



   function handleSignoutClick(event)
   {
     if (confirm('Are you sure you want to logout?'))
     {
       gapi.auth2.getAuthInstance().signOut();
     }
   }



/******************** END AUTHENTICATION ********************/



/******************** DRIVER API ********************/



   function getDriveFiles()
   {
     gapi.client.load('drive', 'v2', getFiles);
   }



   function getFiles()
   {

     var query = '';

     if (ifShowSharedFiles())
     {
       query = (FOLDER_ID == 'root') ? 'trashed=false and sharedWithMe' : "trashed=false and '" + FOLDER_ID + "' in parents";
       if (FOLDER_ID != 'root' && FOLDER_PERMISSION == 'true')
       {
         $('.button-opt').show();
       }
     }
     else
     {
       $('.button-opt').show();
       query = "trashed=false and '" + FOLDER_ID + "' in parents";
     }

     var request = gapi.client.drive.files.list
     (
       {
         'maxResults': NO_OF_FILES,
         'q': query
       }
     );

     request.execute
     (
       function(resp)
       {
         if (!resp.error)
         {
           DRIVE_FILES = resp.items;
         }
         else
         {
           showErrorMessage('Error: ' + resp.error.message);
         }
       }
     );

   }



   function createFolder(name)
   {

     var access_token = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;

     var request = gapi.client.request
     (
       {
         'path'    : '/drive/v2/files/',
         'method'  : 'POST',
         'headers' : {
                       'Content-Type'  : 'application/json',
                       'Authorization' : 'Bearer ' + access_token,
                     },
         'body'    : {
                       'title'    : name,
                       'mimeType' : 'application/vnd.google-apps.folder',
                       'parents'  : [
                                      {
                                        'kind': 'drive#file',
                                        'id': FOLDER_ID
                                      }
                                    ]
                     }
       }
     );

     request.execute
     (
       function(resp)
       {
         if (!resp.error)
         {
           getDriveFiles();
         }
         else
         {
           showErrorMessage('Error: ' + resp.error.message);
         }
       }
     );

   }



   function browseFolder(name)
   {

     FOLDER_NAME       = name;
     FOLDER_ID         = $(obj).attr('data-id');
     FOLDER_LEVEL      = parseInt($(obj).attr('data-level'));
     FOLDER_PERMISSION = $(obj).attr('data-has-permission');

     if (typeof FOLDER_NAME === 'undefined')
     {
       FOLDER_NAME       = '';
       FOLDER_ID         = 'root';
       FOLDER_LEVEL      = 0;
       FOLDER_PERMISSION = true;
       FOLDER_ARRAY      = [];
     }
     else
     {
       if (FOLDER_LEVEL == FOLDER_ARRAY.length && FOLDER_LEVEL > 0)
       {
         // do nothing
       }
       else if (FOLDER_LEVEL < FOLDER_ARRAY.length)
       {
         var tmpArray = cloneObject(FOLDER_ARRAY);
         FOLDER_ARRAY = [];
         for (var i = 0; i < tmpArray.length; i++)
         {
           FOLDER_ARRAY.push(tmpArray[i]);
           if (tmpArray[i].Level >= FOLDER_LEVEL) { break; }
         }
       }
       else
       {
         var fd = {
                    Name: FOLDER_NAME,
                    ID: FOLDER_ID,
                    Level: FOLDER_LEVEL,
                    Permission: FOLDER_PERMISSION
                  }
         FOLDER_ARRAY.push(fd);
       }
     }

     var sbNav = '';

     for (var i = 0; i < FOLDER_ARRAY.length; i++)
     {
       sbNav += "<span class='breadcrumb-arrow'></span>";
       sbNav += "<span class='folder-name'><a data-id='" + FOLDER_ARRAY[i].ID + "' data-level='" + FOLDER_ARRAY[i].Level + "' data-name='" + FOLDER_ARRAY[i].Name + "' data-has-permission='" + FOLDER_PERMISSION + "'>" + FOLDER_ARRAY[i].Name + "</a></span>";
     }

     getDriveFiles();

   }

